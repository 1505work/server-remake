package utl

import (
	"gopkg.in/mgo.v2/bson"
)

//InArray - check if element exists in array
func InArray(val bson.ObjectId, array []bson.ObjectId) (exists bool) {
	exists = false
	for _, v := range array {
		if val == v {
			exists = true
			return
		}
	}
	return
}
