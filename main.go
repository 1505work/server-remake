package main

import (
	"flag"
	"log"
	"net/http"

	"./analyzer"
	"./bot"
	"./firebase"
	"./notifyer"
	s "./structs"

	"github.com/gorilla/sessions"
	mgo "gopkg.in/mgo.v2"
)

var (
	port         string
	sessionStore string
	authSession  string
	appID        string
	iam          string
	secret       string
	token        string
	dataBase     string
	usersC       string
	invitesC     string
	classesC     string
	beaconKey    string
	store        *sessions.CookieStore
	db           *mgo.Database
)

func init() {
	flag.StringVar(&port, "p", "443", "http server port")
	flag.StringVar(&sessionStore, "ss", "abkchdy6gsfthanc", "session secret key")
	flag.StringVar(&authSession, "as", "auth", "auth session name")
	flag.StringVar(&appID, "appid", "6889887", "vk app id")
	flag.StringVar(&iam, "iam", "localhost:8080", "app host address")
	flag.StringVar(&secret, "vkSecret", vksecdef, "vk app secret key")
	flag.StringVar(&token, "vkToken", vktokendef, "vk service token")
	flag.StringVar(&dataBase, "db", "wints", "data base name")
	flag.StringVar(&usersC, "users", "users", "mongo collection name for save users(teachers)")
	flag.StringVar(&invitesC, "invites", "invites", "mongo collection name for save invite links")
	flag.StringVar(&classesC, "classes", "classes", "mongo collection name for save classes")
	flag.StringVar(&beaconKey, "bkey", bkeydef, "password which you must sew up into beacons")
	flag.Parse()
	log.Println("flags parse: ", flag.Parsed())

	store = sessions.NewCookieStore([]byte(sessionStore))
	session, err := mgo.Dial("mongodb://127.0.0.1")
	if err != nil {
		panic(err)
	}
	db = session.DB(dataBase)
}

func main() {
	//HTTP REDIRECT
	go http.ListenAndServe(":80", http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		target := "https://" + req.Host + req.URL.Path
		if len(req.URL.RawQuery) > 0 {
			target += "?" + req.URL.RawQuery
		}
		log.Printf("redirect to: %s", target)
		http.Redirect(w, req, target, http.StatusTemporaryRedirect)
	}))

	//FILES
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "static/index.html")
	})
	http.HandleFunc("/admin", func(w http.ResponseWriter, r *http.Request) {
		session, _ := store.Get(r, authSession)
		if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
			http.Redirect(w, r, "/", 302)
			return
		}
		http.ServeFile(w, r, "static/admin.html")
	})
	http.HandleFunc("/tied", func(w http.ResponseWriter, r *http.Request) {
		session, _ := store.Get(r, authSession)
		if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
			http.Redirect(w, r, "/", 302)
			return
		}
		http.ServeFile(w, r, "static/tied.html")
	})
	http.HandleFunc("/faq", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "static/faq.html")
	})
	http.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "static/login.html")
	})
	http.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "static/res/favicon.png")
	})
	http.HandleFunc("/firebase-messaging-sw.js", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "static/js/firebase-messaging-sw.js")
	})

	//API
	http.HandleFunc("/api/who", WhoRouterHandler)
	http.HandleFunc("/api/generate/invite", GenerateInviteRouterHandler)
	http.HandleFunc("/api/invite/", ProcessInviteRouterHandler)
	http.HandleFunc("/api/add/class", AddClassRouterHandler)
	http.HandleFunc("/api/add/beacon", AddBeaconRouterHandler)
	http.HandleFunc("/api/get/teachers", GetTeachersRouterHandler)
	http.HandleFunc("/api/get/classes", GetClassesRouterHandler)
	http.HandleFunc("/api/get/class", GetClassRouterHandler)
	http.HandleFunc("/api/update/class", UpdateClassRouterHandler)
	http.HandleFunc("/api/update/fbid", RegistrateFBIDRouterHandler)
	http.HandleFunc("/api/updater", UpdaterRouterHandler)
	http.HandleFunc("/api/get/tied", GetTiedRouterHandler)
	http.HandleFunc("/api/remove/class", RemoveClassRouterHandler)

	//Auth(traditional)
	http.HandleFunc("/signup", RegistrateRouterHandler)
	http.HandleFunc("/signin", LoginRouterHandler)
	//OAuth
	http.HandleFunc("/oauth-enter", OAuthEnterRouterHandler)
	http.HandleFunc("/oauth", OAuthRouterHandler)
	http.HandleFunc("/logout", LogoutRouterHandler)

	//FILE SERVER
	fs := http.FileServer(http.Dir(""))
	http.Handle("/static/", fs)

	//PIPELINE
	notifyBot := make(chan s.BotPack)
	bot := bot.NewBot(accesToken, db, notifyBot)
	bot.Start()
	notifyFb := make(chan s.BotPack)
	fb := firebase.NewFirebase(fbtoken, db, notifyFb)
	fb.Start()
	notify := make(chan s.NotifyerPack)
	notifyer := notifyer.NewNotifyer(notify, notifyBot, notifyFb, db)
	notifyer.Start()
	anlzr := analyzer.NewAnalyzer(db, notify)
	anlzr.Start()

	//RUN SERVER
	log.Println("listen: ", port)
	err := http.ListenAndServeTLS(":"+port, "server.crt", "server.key", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
