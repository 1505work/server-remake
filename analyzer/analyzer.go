package analyzer

import (
	"log"
	"time"

	"gopkg.in/mgo.v2/bson"

	cnst "../const"
	s "../structs"

	mgo "gopkg.in/mgo.v2"
)

//Analyzer - just a strct
type Analyzer struct {
	DB *mgo.Database
	O  chan<- s.NotifyerPack
}

func NewAnalyzer(db *mgo.Database, notify chan<- s.NotifyerPack) *Analyzer {
	lz := new(Analyzer)
	lz.DB = db
	lz.O = notify
	return lz
}

//Start - start analyzer
func (lz *Analyzer) Start() {
	log.Println("analyzer started")
	go func() {
		for {
			time.Sleep(time.Minute / 6)
			current, _ := time.Parse("15:04", time.Now().Format("15:04"))
			var classes []s.Class
			lz.DB.C("classes").Find(nil).All(&classes)
			for _, class := range classes {
				start, _ := time.Parse("15:04", class.ActiveStart)
				stop, _ := time.Parse("15:04", class.ActiveStop)
				if (current.After(start) && current.Before(stop)) != class.Active {
					lz.DB.C("classes").UpdateId(class.ID, bson.M{"$set": bson.M{"active": !class.Active}})
					class.Active = !class.Active
					if class.Active {
						if class.State {
							lz.O <- s.NewNotifyerPack(class, cnst.BReport)
							log.Println("problem with class ", class.ID, " reported")
						}
					}
					log.Println("class ", class.ID, " active: ", class.Active)
				}
				cash := class.State
				class.State = false
				for _, beacon := range class.Beacons {
					if beacon.Active && beacon.State {
						class.State = true
						break
					}
				}
				lz.DB.C("classes").UpdateId(class.ID, bson.M{"$set": bson.M{"state": class.State}})
				if cash != class.State && !class.State && !class.Active {
					lz.O <- s.NewNotifyerPack(class, cnst.BThank)
					log.Println("problem with class ", class.ID, " solved")
				}
				if cash != class.State {
					log.Println("class ", class.ID, " state: ", class.State)
				}
				if class.State && !class.Active {
					lz.O <- s.NewNotifyerPack(class, cnst.BAlert)
				}
			}
		}
	}()
}
