// firebase_subscribe.js
firebase.initializeApp({
    messagingSenderId: '822402227971'
});

// браузер поддерживает уведомления
// вообще, эту проверку должна делать библиотека Firebase, но она этого не делает
if ('Notification' in window) {
    var messaging = firebase.messaging();

    messaging.onMessage(function(payload) {
        // sendNotification('WINTS', {
        //     body: payload.notification,
        //     icon: '../res/favicon.png',
        //     dir: 'auto'
        //     });
        console.log("new notify!!")
        alert("Проверьте состояния окон в своем личном кабинете!")
    });

    // пользователь уже разрешил получение уведомлений
    // подписываем на уведомления если ещё не подписали
    // if (Notification.permission === 'granted') {
    //     subscribe();
    // }

    // по клику, запрашиваем у пользователя разрешение на уведомления
    // и подписываем его
    $('#subscribe').on('click', function () {
        subscribe();
    });
}

function subscribe() {
    // запрашиваем разрешение на получение уведомлений
    messaging.requestPermission()
        .then(function () {
            // получаем ID устройства
            messaging.getToken()
                .then(function (currentToken) {
                    console.log(currentToken);

                    if (currentToken) {
                        sendTokenToServer(currentToken);
                    } else {
                        console.warn('Не удалось получить токен.');
                        setTokenSentToServer(false);
                    }
                })
                .catch(function (err) {
                    console.warn('При получении токена произошла ошибка.', err);
                    setTokenSentToServer(false);
                });
    })
    .catch(function (err) {
        console.warn('Не удалось получить разрешение на показ уведомлений.', err);
    });
}

// отправка ID на сервер
async function sendTokenToServer(currentToken) {
    if (!nav.subscribed && nav.loggedIn) {
        console.log('Отправка токена на сервер...');

        await fetch("/api/update/fbid", {
            method:"post",
            body: JSON.stringify({
                id: currentToken
            })
        })

        setTokenSentToServer(currentToken);
        window.location.reload()
    } else {
        console.log('Токен уже отправлен на сервер.');
    }
}

// используем localStorage для отметки того,
// что пользователь уже подписался на уведомления
function isTokenSentToServer(currentToken) {
    return window.localStorage.getItem('sentFirebaseMessagingToken') == currentToken;
}

function setTokenSentToServer(currentToken) {
    window.localStorage.setItem(
        'sentFirebaseMessagingToken',
        currentToken ? currentToken : ''
    );
}