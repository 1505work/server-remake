window.mobilecheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};

var nav = new Vue({
    el: "#nav",
    data: {
        loggedIn: false,
        isAdmin: false,
        name: "",
        mobile: false,
        open: false,
        animname: "navanim"
    },
    methods: {
        logout: function() {
            fetch("/logout").then(function(resp) {
                if (resp.ok) window.location.replace("/");
            })
        }
    },
    watch: {
        mobile: function() {
            this.animname = !this.mobile ? "navanim" : "navanimmob";
            if (this.mobile) {
                document.body.onload = () => {document.getElementById("vk_community_messages").style.transform = "scale(1.5) translate(-25%, -25%)"}
            }
        }
    },
    created: function() {
        this.mobile = window.mobilecheck()
        fetch("/api/who", {method: "get"}).then((resp) => {
            return resp.json()
        }).then((json) => {
            if (json != undefined) {
                this.loggedIn = true
                this.isAdmin = json.admin
                if (!json.admin) window.location.replace("/")
                this.name = json.name
            }
        })
    }
})

document.body.onclick = function(event) {
    var el = event.target.parentNode
    while (el != undefined) {
        if (el.id == "nav") return 
        el = el.parentNode
    }
    nav.open = false;
}

var notifier = new Vue({
    el: "#notifies",
    data: {
        stack: []
    },
    methods: {
        notify: function(message) {
            this.stack.push(message)
            setTimeout(this.clear, 2000)
        },
        clear: function() {
            if (this.stack.length != 0) {
                this.stack.shift()
            }
        }
    }
})

var menu = new Vue({
    el: "#menu",
    data: {
        show: true
    },
    methods: {
        newLink: async function() {
            var copyText = document.getElementById("clipboard");
            await fetch("/api/generate/invite", {method:"get"}).then((resp) => {
                return resp.text()
            }).then((text) => {
                copyText.value = text
            })
            copyText.select();
            document.execCommand("copy");
            notifier.notify("Ссылка скопированна в буфер обмена")
        },
        openManage: function() {
            this.show = false
            manage.show = true
        },
        openAddClass: function() {
            this.show = false;
            addClass.show = true;
        },
        openAddBeacon: function() {
            this.show = false
            addBeacon.show = true
        }
    }
})

var addClass = new Vue({
    el: "#addClass",
    data: {
        show: false,
        name: "",
        start: "",
        stop: ""
    },
    methods: {
        back: function() {
            window.location.reload()
        },
        ready: function() {
            fetch("/api/add/class", {
                method: "post",
                body: JSON.stringify({
                    name: this.name,
                    start: this.start,
                    stop: this.stop
                })
            }).then((resp) => {
                window.location.reload()
            })
        }
    }
})

var addBeacon = new Vue({
    el: "#addBeacon",
    data: {
        show: false,
        state: false,
        name: "",
        classes: [],
        classId: ""
    },
    created: function() {
        fetch("/api/get/classes", {method: "get"}).then((resp) => {
            return resp.json()
        }).then((json) => {
            this.classes = json
        })
    },
    methods: {
        back: function() {
            window.location.reload()
        },
        ready: function() {
            fetch("/api/add/beacon", {
                method: "post",
                body: JSON.stringify({
                    classId: this.classId,
                    name: this.name,
                    state: this.state
                })
            }).then((resp) => {
                window.location.reload()
            })
        }
    }
})

var manage = new Vue({
    el: "#manage",
    data: {
        show: false,
        beacons: [],
        teachers: [],
        classes: [],
        classId: "",
        name: "",
        start: "",
        stop: ""
    },
    created: function() {
        fetch("/api/get/classes", {method: "get"}).then((resp) => {
            return resp.json()
        }).then((json) => {
            this.classes = json
        })
        fetch("/api/get/teachers", {method: "get"}).then((resp) => {
            return resp.json()
        }).then((json) => {
            this.teachers = json
        })
    },
    methods: {
        back: function() {
            window.location.reload()
        },
        ready: function() {
            var teachers = []
            for (var i = 0; i < this.teachers.length; i++) {
                if (this.teachers[i].added) teachers.push(this.teachers[i].id)
            }
            fetch("/api/update/class", {
                method: "post",
                body: JSON.stringify({
                    id: this.classId,
                    beacons: this.beacons,
                    teachers: teachers,
                    name: this.name,
                    activeStart: this.start,
                    activeStop: this.stop
                })
            }).then(() => {
                window.location.reload()
            })
        },
        remove: function() {
            fetch("/api/remove/class", {
                method: "post",
                body: JSON.stringify({
                    id: this.classId
                })
            }).then(() => {
                window.location.reload()
            })
        }
    },
    watch: {
        classId: function() {
            fetch("/api/get/class", {
                method: "post",
                body: JSON.stringify({
                    id: this.classId
                })
            }).then((resp) => {
                return resp.json()
            }).then((json) => {
                this.beacons = json.beacons
                for (var i = 0; i < this.teachers.length; i++) {
                    this.teachers[i].added = json.teachers.join("").indexOf(this.teachers[i].id) != -1
                }
                this.name = json.name
                this.start = json.activeStart
                this.stop = json.activeStop
            })
        }
    }
})