var fs = 15;

var width = 50; 
var height = 50; 
var field = document.getElementById("view"); 
var snake = [ 
        [0, 0]
    ]; 
var food = [10, 10]; 
var direction = [1, 0]; 

function draw() { 
    field.innerHTML = ""; 
    for (var i = 0; i < height; i++) { 
        for (var j = 0; j < width; j++) { 
            if (("|" + snake.join("|") + "|").indexOf("|" + j + "," + i + "|") != -1) {
                field.innerHTML += "■";
            } else if (j == food[0] && i == food[1]) { 
                field.innerHTML += "■";
            } else field.innerHTML += "□"; 
        } 
        field.innerHTML += "<br>"; 
    } 
} 

function move() { 
    for (var i = snake.length - 1; i >= 0; i--) { 
        if (i != 0) { 
            snake[i][0] = snake[i - 1][0] 
            snake[i][1] = snake[i - 1][1] 
        } else { 
            snake[i][0] += direction[0] 
            snake[i][1] += direction[1] 
        } 
    } 
} 

function eat() { 
    if (food[0] == snake[0][0] && food[1] == snake[0][1]) { 
        snake.push([snake[snake.length - 1][0], snake[snake.length - 1][1]]) 
        do {
            food[0] = getRandomInt(0, width - 1); 
            food[0] = getRandomInt(0, height - 1); 
        } while (("|" + snake.join("|") + "|").indexOf("|" + food[0] + "," + food[1] + "|") != -1)
    } 
} 

function die() {
    if (snake[0][0] < 0 || snake[0][0] >= width || snake[0][1] < 0 || snake[0][1] >= height || (("|" + snake.subarray(1, -1).join("|") + "|").indexOf("|" + snake[0][0] + "," + snake[0][1] + "|") != -1)) {
        document.body.style.background = "red"
        clearInterval(game)
    }
}

var game = setInterval(() => { 
    eat() 
    move() 
    die()
    draw() 
}, 1) 

window.onkeydown = function() { 
    switch (event.keyCode) { 
        case 37: 
            if (direction[0] == 1) break; 
            direction = [-1, 0]; 
            break; 
        case 38: 
            if (direction[1] == 1) break; 
            direction = [0, -1]; 
            break; 
        case 39: 
            if (direction[0] == -1) break; 
            direction = [1, 0]; 
            break; 
        case 40: 
            if (direction[1] == -1) break; 
            direction = [0, 1]; 
            break; 
    } 
} 

function getRandomInt(min, max) { return Math.min(max, Math.floor(Math.random() * (max - min + 1) + min)); }

Array.prototype.subarray=function(start,end){
    if(!end){ end=-1;} 
   return this.slice(start, this.length+1-(end*-1));
}

var touchstartX = 0;
var touchstartY = 0;
var touchendX = 0;
var touchendY = 0;

var gesuredZone = document.body;

gesuredZone.addEventListener('touchstart', function(event) {
    touchstartX = event.screenX;
    touchstartY = event.screenY;
}, false);

gesuredZone.addEventListener('touchend', function(event) {
    touchendX = event.screenX;
    touchendY = event.screenY;
    handleGesure();
}, false); 

function handleGesure() {
    var swiped = 'swiped: ';
    if (touchendX < touchstartX) {
        if (direction[0] == 1) direction = [-1, 0]; 
        return;
    }
    if (touchendX > touchstartX) {
        if (direction[0] == 1) direction = [-1, 0]; 
        return;
    }
    if (touchendY < touchstartY) {
        if (direction[1] == -1) direction = [0, 1]; 
        return;
    }
    if (touchendY > touchstartY) {
        if (direction[1] == 1) direction = [0, -1];
        return;
    }
}