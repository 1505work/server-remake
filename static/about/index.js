var tp1, tp2, tp3, tp4
var sound = false

var slide1 = new Vue({
    el: "#slide1",
    data: {
        show: true
    },
    created: function() {
        document.body.style = "#e67e22";
        setTimeout(function() {
            tp1 = new LeaderLine(
                document.getElementById('title'),
                document.getElementById('problem1')
            );    
            tp1.color = "#2c3e50";
            tp1.hide("none")
            tp1.show("draw")
            tp2 = new LeaderLine(
                document.getElementById('title'),
                document.getElementById('problem2')
            );    
            tp2.color = "#2c3e50";
            tp2.hide("none")
            tp2.show("draw")
            tp3 = new LeaderLine(
                document.getElementById('title'),
                document.getElementById('problem3')
            );    
            tp3.color = "#2c3e50";
            tp3.hide("none")
            tp3.show("draw")
            tp4 = new LeaderLine(
                document.getElementById('title'),
                document.getElementById('problem4')
            );    
            tp4.color = "#2c3e50";
            tp4.hide("none")
            tp4.show("draw")
            if (sound) new Audio('res/draw.mp3').play();
        }, 1500);
    },
    methods: {
        openSlide: function(event, opener) {
            var target = event.target
            if (event.target.tagName != "DIV") {
                target = event.target.parentNode
            }
            document.getElementById("none-touch").remove()
            setTimeout(() => {
                target.innerHTML = "";
                target.style.zIndex = "1000";
                target.style.transform = "scale(30)";
                target.style.borderColor = "#1abc9c00";
                setTimeout(() => {
                    tp1.remove()
                    tp2.remove()
                    tp3.remove()
                    tp4.remove()
                    this.show = false;
                    document.getElementById("back").style.display = "block";
                    opener()
                }, 1200)
            }, 10)
        },
        p1: function(event) {
            this.openSlide(event, () => {slide2.show = true;})
        },
        p2: function(event) {
            this.openSlide(event, () => {slide3.show = true;})
        },
        p3: function(event) {
            this.openSlide(event, () => {slide4.show = true;})
        },
        p4: function(event) {
            this.openSlide(event, () => {slide5.show = true;})
        }
    }
})

var slide2 = new Vue({
    el:"#slide2",
    data: {
        show: false
    },
    watch: {
        show: function() {
            if (this.show) {
                document.body.style.background = "#e74c3c";
                document.body.style.overflowY = "scroll";
                if (sound) new Audio('res/slide.mp3').play();
            }
        }
    }
})

var slide3 = new Vue({
    el: "#slide3",
    data: {
        show: false
    },
    watch: {
        show: function() {
            if (this.show) {
                document.body.style.background = "#27ae60";
                document.body.style.overflowY = "scroll";
                if (sound) new Audio('res/nameplate.mp3').play();
                setTimeout(() => {
                    document.getElementById("plate-animations").remove()
                }, 750)
                var plates = document.getElementsByClassName("nameplate")
                for (var i = 0; i < plates.length; i++) {
                    plates[i].onmouseover = (event) => {
                        event.target.classList.add("animate")
                        if (sound) new Audio('res/nameplate-hov.mp3').play();
                    }
                    plates[i].addEventListener("animationend", (event) => {
                        event.target.classList.remove("animate")
                    });
                }
            }
        }
    }
})

var slide4 = new Vue({
    el:"#slide4",
    data: {
        show: false
    },
    watch: {
        show: function() {
            if (this.show) document.body.style.background = "#2980b9";
            if (sound) {
                var kick = new Audio('res/ball.mp3');
                var vol = 1;
                var kicker = setInterval(() => {
                    kick.volume = vol
                    vol -= 0.05;
                    kick.play();
                    if (vol == 0) clearInterval(kicker)
                }, 350)
            }
        }
    }
})

var slide5 = new Vue({
    el:"#slide5",
    data: {
        show: false
    },
    watch: {
        show: function() {
            if (this.show) {
                document.body.style.background = "#2980b9";
                document.body.style.overflowY = "scroll";
            }
            setTimeout(drawArhConnections, 700);
        }
    }
})

window.onload = () => {
    setTimeout(() => {
        document.getElementById("animations").remove()
    }, 300)
    if (sound) new Audio('res/bubble.mp3').play();
}

function drawArhConnections() {
    var beacons = document.getElementsByClassName("fa-tachometer-alt")
    for (var i = 0; i < beacons.length; i++) {
        new LeaderLine(
            beacons[i],
            document.getElementById('rest'), 
            {color: "#2c3e50", path: "grid", hide: true}
        ).show("draw");    
    }
    new LeaderLine(
        document.getElementById("analyzer"),
        document.getElementById('notifyer'), 
        {color: "#2c3e50", path: "straight", hide: true}
    ).show("draw");
    new LeaderLine(
        LeaderLine.pointAnchor(document.getElementById("notifyer"), {x: '50%', y: "100%"}),
        LeaderLine.pointAnchor(document.getElementById("vk"), {x: '50%', y: "0%"}),
        {color: "#2c3e50", path: "straight", hide: true}
    ).show("draw");
    new LeaderLine(
        LeaderLine.pointAnchor(document.getElementById("notifyer"), {x: '50%', y: "100%"}),
        document.getElementById('fb'), 
        {color: "#2c3e50", path: "straight", hide: true}
    ).show("draw");
    new LeaderLine(
        document.getElementById('frontapi'),
        document.getElementById('site'), 
        {color: "#2c3e50", path: "grid", hide: true}
    ).show("draw"); 
    new LeaderLine(
        document.getElementById('firebase'),
        document.getElementById('pwa'), 
        {color: "#2c3e50", path: "grid", hide: true}
    ).show("draw"); 
    new LeaderLine(
        document.getElementById('pwa'),
        document.getElementById('site'), 
        {color: "#2c3e50", path: "grid", hide: true}
    ).show("draw"); 
    new LeaderLine(
        document.getElementById('fb'),
        document.getElementById('firebase'), 
        {color: "#2c3e50", path: "grid", hide: true}
    ).show("draw"); 
    new LeaderLine(
        document.getElementById('vk'),
        document.getElementById('vkapi'), 
        {color: "#2c3e50", path: "grid", hide: true}
    ).show("draw"); 
    new LeaderLine(
        document.getElementById('mongo'),
        document.getElementById('rest'), 
        {color: "#2c3e50", path: "grid", hide: true}
    ).show("draw"); 
    new LeaderLine(
        document.getElementById('mongo'),
        document.getElementById('frontapi'), 
        {color: "#2c3e50", path: "grid", hide: true}
    ).show("draw"); 
    new LeaderLine(
        document.getElementById('mongo'),
        document.getElementById('analyzer'), 
        {color: "#2c3e50", path: "grid", hide: true}
    ).show("draw"); 
    new LeaderLine(
        document.getElementById('mongo'),
        document.getElementById('notifyer'), 
        {color: "#2c3e50", path: "grid", hide: true}
    ).show("draw"); 
}

  
try {  
    document.createEvent("TouchEvent");  
    document.getElementById("none-touch").remove()
} catch (e) {  
}  

function full() {
    document.documentElement.requestFullscreen();
    document.getElementById("full").innerHTML = "<i class='fas fa-compress-arrows-alt'></i>";
    document.getElementById("full").onclick =() => {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
        document.getElementById("full").innerHTML = "<i class='fas fa-compress'></i>";   
        document.getElementById("full").onclick = full;
    }
}