package main

import (
	"crypto/sha1"
	"encoding/base64"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	s "./structs"
	"./utl"

	"gopkg.in/mgo.v2/bson"
)

//Auth

//OAUTH

//OAuthEnterRouterHandler - enter to oauth
func OAuthEnterRouterHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, authSession)
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Redirect(w, r, "https://oauth.vk.com/authorize?client_id="+appID+"&display=page&redirect_uri=https://"+iam+"/oauth&scope=&response_type=code&v=5.87", 302)
		return
	}
	http.Redirect(w, r, "/", 302)
}

//OAuthRouterHandler - vk oauth
func OAuthRouterHandler(w http.ResponseWriter, r *http.Request) {
	resp, _ := http.Get("https://oauth.vk.com/access_token?client_id=" + appID + "&client_secret=" + secret + "&redirect_uri=https://" + iam + "/oauth&code=" +
		r.FormValue("code"))
	var requestOAuth struct {
		ID    int    `json:"user_id"`
		Token string `json:"access_token"`
	}
	if err := json.NewDecoder(resp.Body).Decode(&requestOAuth); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	resp, _ = http.Get("https://api.vk.com/method/users.get?user_ids=" + strconv.Itoa(requestOAuth.ID) + "&fields=&access_token=" + token + "&v=5.87")
	var requestInfo struct {
		Users []struct {
			ID int    `json:"id"`
			FN string `json:"first_name"`
			LN string `json:"last_name"`
		} `json:"response"`
	}
	if err := json.NewDecoder(resp.Body).Decode(&requestInfo); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	session, _ := store.Get(r, authSession)
	session.Values["authenticated"] = true
	session.Values["login"] = strconv.Itoa(requestOAuth.ID)
	err := session.Save(r, w)
	if err != nil {
		log.Println(err)
	}

	lengt, err := db.C(usersC).Find(bson.M{"vk": strconv.Itoa(requestOAuth.ID)}).Count()
	if err != nil {
		w.Write([]byte("<h1>SMTH WENT WRONG :`(</h1>"))
		return
	}
	if lengt == 0 {
		db.C(usersC).Insert(s.Teacher{
			Name:       requestInfo.Users[0].FN + " " + requestInfo.Users[0].LN,
			ID:         bson.NewObjectId(),
			VK:         strconv.Itoa(requestInfo.Users[0].ID),
			Admin:      false,
			Confirmed:  false,
			VKED:       true,
			Subscribed: false})
	}
	http.Redirect(w, r, "/", 302)
}

//Custom AUTH

//RegistrateRouterHandler - traditional regestration
func RegistrateRouterHandler(w http.ResponseWriter, r *http.Request) {
	var request struct {
		Login    string `json:"login"`
		Password string `json:"password"`
		Name     string `json:"name"`
	}
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Println(request)
	lengt, err := db.C(usersC).Find(bson.M{"$or": []bson.M{bson.M{"login": request.Login}, bson.M{"name": request.Name}}}).Count()
	if err != nil {
		http.Error(w, "error check data", http.StatusInternalServerError)
		return
	}
	log.Println(lengt)
	if lengt == 0 {
		if len(request.Password) > 6 {
			hasher := sha1.New()
			hasher.Write([]byte(request.Password))
			sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
			db.C(usersC).Insert(s.Teacher{
				Name:       request.Name,
				ID:         bson.NewObjectId(),
				VK:         request.Login,
				Login:      request.Login,
				Password:   sha,
				Admin:      false,
				Confirmed:  true,
				VKED:       false,
				Subscribed: false})
			session, _ := store.Get(r, authSession)
			session.Values["authenticated"] = true
			session.Values["login"] = request.Login
			err := session.Save(r, w)
			if err != nil {
				log.Println(err)
			}
			return
		}
		http.Error(w, "short", http.StatusInternalServerError)
		return
	}
	http.Error(w, "busy", http.StatusInternalServerError)
}

//LoginRouterHandler - login via password and login
func LoginRouterHandler(w http.ResponseWriter, r *http.Request) {
	var request struct {
		Login    string `json:"login"`
		Password string `json:"password"`
	}
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	var user s.Teacher
	db.C(usersC).Find(bson.M{"login": request.Login}).One(&user)
	hasher := sha1.New()
	hasher.Write([]byte(request.Password))
	sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
	if !user.VKED && user.Password == sha {
		session, _ := store.Get(r, authSession)
		session.Values["authenticated"] = true
		session.Values["login"] = request.Login
		session.Save(r, w)
	}
}

//LogoutRouterHandler - logout
func LogoutRouterHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, authSession)
	session.Options.MaxAge = -1
	session.Save(r, w)
}

//API

//WhoRouterHandler - api gate to get authed user's info
func WhoRouterHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, authSession)
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "auth error", http.StatusInternalServerError)
		return
	}

	var user s.Teacher
	db.C(usersC).Find(bson.M{"vk": session.Values["login"]}).One(&user)
	js, err := json.Marshal(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

//GenerateInviteRouterHandler - generate and save new invite link
func GenerateInviteRouterHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, authSession)
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "auth error", http.StatusInternalServerError)
		return
	}

	var user s.Teacher
	db.C(usersC).Find(bson.M{"vk": session.Values["login"]}).One(&user)

	if user.Admin {
		hasher := sha1.New()
		hasher.Write([]byte(time.Now().Format("2006-01-02T15:04:05.999999-07:00")))
		sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
		err := db.C(invitesC).Insert(s.Invite{Key: sha})
		if err != nil {
			http.Error(w, "db error: "+err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write([]byte("https://" + iam + "/api/invite/" + sha))
		log.Println("new invite generated by " + user.Name)
	} else {
		http.Error(w, "access error", http.StatusInternalServerError)
	}
}

//ProcessInviteRouterHandler - update user to admin by invite
func ProcessInviteRouterHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, authSession)
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "auth error", http.StatusInternalServerError)
		return
	}

	key := strings.Split(r.URL.Path, "/")[3]
	err := db.C(invitesC).Remove(bson.M{"key": key})
	if err != nil {
		http.Error(w, "not valid link", http.StatusInternalServerError)
	}

	db.C(usersC).Update(bson.M{"vk": session.Values["login"]}, bson.M{"$set": bson.M{"admin": true}})
	log.Println("New ADMIN in de house ", session.Values["login"])
	http.Redirect(w, r, "/", 302)
}

//AddClassRouterHandler - add new class
func AddClassRouterHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, authSession)
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "auth error", http.StatusInternalServerError)
		return
	}

	var user s.Teacher
	db.C(usersC).Find(bson.M{"vk": session.Values["login"]}).One(&user)

	if user.Admin {
		var request struct {
			Name        string `json:"name"`
			ActiveStart string `json:"start"`
			ActiveStop  string `json:"stop"`
		}
		err := json.NewDecoder(r.Body).Decode(&request)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		// start, _ := time.Parse(request.ActiveStart, "15:04")
		// stop, _ := time.Parse(request.ActiveStop, "15:04")
		err = db.C(classesC).Insert(s.Class{
			ID:          bson.NewObjectId(),
			Name:        request.Name,
			ActiveStart: request.ActiveStart,
			ActiveStop:  request.ActiveStop})
		if err == nil {
			log.Println("new class added ", request.Name)
		} else {
			log.Println("add class error", err.Error())
		}
	} else {
		http.Error(w, "access error", http.StatusInternalServerError)
	}
}

//AddBeaconRouterHandler - add new beacon
func AddBeaconRouterHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, authSession)
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "auth error", http.StatusInternalServerError)
		return
	}

	var user s.Teacher
	db.C(usersC).Find(bson.M{"vk": session.Values["login"]}).One(&user)

	if user.Admin {
		var request struct {
			ClassID string `json:"classId"`
			Name    string `json:"name"`
			State   bool   `json:"state"`
		}
		err := json.NewDecoder(r.Body).Decode(&request)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		err = db.C(classesC).UpdateId(bson.ObjectIdHex(request.ClassID), bson.M{"$push": bson.M{"beacons": s.Beacon{
			ID:     bson.NewObjectId(),
			Name:   request.Name,
			State:  request.State,
			Active: true}}})
		if err == nil {
			log.Println("new beacon added ", request.Name)
		} else {
			log.Println("add beacon error", err.Error())
		}
	} else {
		http.Error(w, "access error", http.StatusInternalServerError)
	}
}

//GetTeachersRouterHandler - get all beacons
func GetTeachersRouterHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, authSession)
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "auth error", http.StatusInternalServerError)
		return
	}

	var user s.Teacher
	db.C(usersC).Find(bson.M{"vk": session.Values["login"]}).One(&user)

	if user.Admin {
		var teachers []s.Teacher
		db.C(usersC).Find(bson.M{}).All(&teachers)
		js, err := json.Marshal(teachers)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	} else {
		http.Error(w, "access error", http.StatusInternalServerError)
	}
}

//GetClassesRouterHandler - get all beacons
func GetClassesRouterHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, authSession)
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "auth error", http.StatusInternalServerError)
		return
	}

	var user s.Teacher
	db.C(usersC).Find(bson.M{"vk": session.Values["login"]}).One(&user)

	if user.Admin {
		var classes []s.Class
		db.C(classesC).Find(bson.M{}).All(&classes)
		js, err := json.Marshal(classes)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	} else {
		http.Error(w, "access error", http.StatusInternalServerError)
	}
}

//GetClassRouterHandler - get all beacons
func GetClassRouterHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, authSession)
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "auth error", http.StatusInternalServerError)
		return
	}

	var user s.Teacher
	db.C(usersC).Find(bson.M{"vk": session.Values["login"]}).One(&user)

	if user.Admin {
		var request struct {
			ID string `json:"id"`
		}
		err := json.NewDecoder(r.Body).Decode(&request)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		var class s.Class
		db.C(classesC).FindId(bson.ObjectIdHex(request.ID)).One(&class)
		js, err := json.Marshal(class)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	} else {
		http.Error(w, "access error", http.StatusInternalServerError)
	}
}

// UpdateClassRouterHandler - update class by id
func UpdateClassRouterHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, authSession)
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "auth error", http.StatusInternalServerError)
		return
	}

	var user s.Teacher
	db.C(usersC).Find(bson.M{"vk": session.Values["login"]}).One(&user)

	if user.Admin {
		var request struct {
			ID          string          `json:"id"`
			Beacons     []s.Beacon      `json:"beacons"`
			Teachers    []bson.ObjectId `json:"teachers"`
			Name        string          `json:"name"`
			ActiveStart string          `json:"activeStart"`
			ActiveStop  string          `json:"activeStop"`
		}
		err := json.NewDecoder(r.Body).Decode(&request)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		err = db.C(classesC).UpdateId(bson.ObjectIdHex(request.ID), bson.M{"$set": bson.M{
			"beacons":     request.Beacons,
			"teachers":    request.Teachers,
			"name":        request.Name,
			"activeStart": request.ActiveStart,
			"activeStop":  request.ActiveStop}})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Println("class updated ", request.ID)
	} else {
		http.Error(w, "access error", http.StatusInternalServerError)
	}
}

//UpdaterRouterHandler - router to update beacons states
func UpdaterRouterHandler(w http.ResponseWriter, r *http.Request) {
	var request struct {
		Class string `json:"class"`
		Name  string `json:"name"`
		State bool   `json:"state"`
		Key   string `json:"key"`
	}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if request.Key == beaconKey {
		err = db.C(classesC).Update(bson.M{"name": request.Class, "beacons.name": request.Name}, bson.M{"$set": bson.M{"beacons.$.state": request.State}})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Println("beacon ", request.Name, " for class ", request.Class, " updated to value ", request.State)
	} else {
		http.Error(w, "wrong key", http.StatusInternalServerError)
	}
}

//GetTiedRouterHandler - get classes tied to user
func GetTiedRouterHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, authSession)
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "auth error", http.StatusInternalServerError)
		return
	}
	var user s.Teacher
	db.C(usersC).Find(bson.M{"vk": session.Values["login"]}).One(&user)
	var classes []s.Class
	db.C(classesC).Find(bson.M{}).All(&classes)
	var response struct {
		Classes []s.Class `json:"classes"`
	}
	for _, class := range classes {
		if utl.InArray(user.ID, class.Teachers) {
			response.Classes = append(response.Classes, class)
		}
	}
	js, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

//RemoveClassRouterHandler - remove class
func RemoveClassRouterHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, authSession)
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "auth error", http.StatusInternalServerError)
		return
	}

	var user s.Teacher
	db.C(usersC).Find(bson.M{"vk": session.Values["login"]}).One(&user)

	if user.Admin {
		var request struct {
			ID string `json:"id"`
		}
		err := json.NewDecoder(r.Body).Decode(&request)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		err = db.C(classesC).RemoveId(bson.ObjectIdHex(request.ID))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Println("class removed ", request.ID)
	} else {
		http.Error(w, "access error", http.StatusInternalServerError)
	}
}

//RegistrateFBIDRouterHandler - registrate firebase id for user
func RegistrateFBIDRouterHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, authSession)
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "auth error", http.StatusInternalServerError)
		return
	}
	var request struct {
		ID string `json:"id"`
	}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	db.C(usersC).Update(bson.M{"vk": session.Values["login"]}, bson.M{"$set": bson.M{"fbid": request.ID, "subscribed": true}})
	log.Println("fbid ", session.Values["login"], " firebase updated ", request.ID)
}
