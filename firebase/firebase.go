package firebase

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	cnst "../const"
	s "../structs"
	mgo "gopkg.in/mgo.v2"
)

//Firebase structure
type Firebase struct {
	token string
	i     <-chan s.BotPack
}

//NewFirebase - create new firebase
func NewFirebase(token string, db *mgo.Database, notify <-chan s.BotPack) *Firebase {
	firebase := new(Firebase)
	firebase.token = token
	firebase.i = notify
	return firebase
}

//Start - strat fb notifyer
func (f *Firebase) Start() {
	go func() {
		log.Println("notifyer in firebase started")
		for pack := range f.i {
			log.Println("fb: ", pack)
			switch pack.Command {
			case cnst.BAlert:
				f.send(pack.Teacher.FireBaseID, "Здравствуйте "+pack.Teacher.Name+", вы забыли закрыть окно в кабинете "+pack.Class, pack.Teacher.VK)
			case cnst.BThank:
				f.send(pack.Teacher.FireBaseID, "Спасибо, состояние кабинета "+pack.Class+" нормализованно", pack.Teacher.VK)
			case cnst.BReport:
				f.send(pack.Teacher.FireBaseID, "Окна в кабинете "+pack.Class+" не были закрыты, администратор уведомлен об этой проблеме.", pack.Teacher.VK)
			case cnst.BReportAdmin:
				f.send(pack.Teacher.FireBaseID, "Окна в кабинете "+pack.Class+" не были закрыты, список учителей, ответственных за этот кабинет вы можете увидеть во вкладке администрирования классов.", pack.Teacher.VK)
			}
		}
	}()
}

func (f *Firebase) send(id string, message string, vk string) {
	var body struct {
		Notification struct {
			Title  string `json:"title"`
			Body   string `json:"body"`
			Action string `json:"click_actio"`
			Icon   string `json:"icon"`
		} `json:"notification"`
		To   string `json:"to"`
		User string `json:"vk"`
	}
	body.Notification.Body = message
	body.Notification.Title = "WINTS"
	body.Notification.Action = "https://ivanchgeek.ru"
	body.Notification.Icon = "https://ivanchgeek.ru/static/res/favicon.png"
	body.User = vk
	body.To = id
	jsbody, _ := json.Marshal(body)
	request, _ := http.NewRequest("POST", "https://fcm.googleapis.com/fcm/send", bytes.NewBuffer(jsbody))
	request.Header.Set("Authorization", "key="+f.token)
	request.Header.Set("Content-Type", "application/json")
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		log.Println(err)
	}
	defer response.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(response.Body)
	log.Println(string(bodyBytes))
}
