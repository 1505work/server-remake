package bot

import (
	"log"
	"strconv"

	cnst "../const"
	s "../structs"

	vkapi "github.com/Dimonchik0036/vk-api"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//Bot - bot structure
type Bot struct {
	db    *mgo.Database
	token string
	i     <-chan s.BotPack
}

//NewBot - create new bot
func NewBot(token string, db *mgo.Database, notify <-chan s.BotPack) *Bot {
	bot := new(Bot)
	bot.db = db
	bot.token = token
	bot.i = notify
	return bot
}

//Start - start bot
func (b Bot) Start() {
	go func() {
		client, err := vkapi.NewClientFromToken(b.token)
		if err != nil {
			log.Panic(err)
		}

		if err := client.InitLongPoll(0, 2); err != nil {
			log.Panic(err)
		}

		updates, _, err := client.GetLPUpdatesChan(100, vkapi.LPConfig{25, vkapi.LPModeAttachments})
		if err != nil {
			log.Println(err)
		}

		log.Println("bot started")
		go b.startNotifying()

		for update := range updates {
			if update.Message == nil || !update.IsNewMessage() || update.Message.Outbox() {
				continue
			}
			if update.Message.Text == "/start" || update.Message.Text == "привет" {
				var user s.Teacher
				b.db.C("users").Find(bson.M{"vk": strconv.FormatInt(update.Message.FromID, 10)}).One(&user)
				if !user.Confirmed {
					b.db.C("users").UpdateId(user.ID, bson.M{"$set": bson.M{"confirmed": true}})
					log.Println(user.Name, " confirmed")
					client.SendMessage(vkapi.NewMessage(vkapi.NewDstFromUserID(update.Message.FromID), "Вы активировали аккаунт"))
				} else {
					client.SendMessage(vkapi.NewMessage(vkapi.NewDstFromUserID(update.Message.FromID), "Ваш аккаунт уже активен"))
				}
				continue
			}
			client.SendMessage(vkapi.NewMessage(vkapi.NewDstFromUserID(update.Message.FromID), "Мне нечего на это ответить."))
		}
	}()
}

func (b *Bot) startNotifying() {
	log.Println("notifyer in bot started")
	for pack := range b.i {
		log.Println("bot: ", pack)
		switch pack.Command {
		case cnst.BAlert:
			id, _ := strconv.ParseInt(pack.Teacher.VK, 10, 64)
			b.send(id, "Здравствуйте, вы забыли закрыть окно в кабинете "+pack.Class)
		case cnst.BThank:
			id, _ := strconv.ParseInt(pack.Teacher.VK, 10, 64)
			b.send(id, "Спасибо, состояние кабинета "+pack.Class+" нормализованно")
		case cnst.BReport:
			id, _ := strconv.ParseInt(pack.Teacher.VK, 10, 64)
			b.send(id, "Окна в кабинете "+pack.Class+" не были закрыты, администратор уведомлен об этой проблеме.")
		case cnst.BReportAdmin:
			id, _ := strconv.ParseInt(pack.Teacher.VK, 10, 64)
			b.send(id, "Окна в кабинете "+pack.Class+" не были закрыты, список учителей, ответственных за этот кабинет вы можете увидеть во вкладке администрирования классов.")
		}
	}
}

func (b Bot) send(id int64, message string) {
	client, _ := vkapi.NewClientFromToken(b.token)
	client.SendMessage(vkapi.NewMessage(vkapi.NewDstFromUserID(id), message))
}
