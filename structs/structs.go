package s

import (
	"gopkg.in/mgo.v2/bson"
)

//Class - class room structure
type Class struct {
	Name        string          `json:"name" bson:"name"`
	ID          bson.ObjectId   `json:"id" bson:"_id"`
	Teachers    []bson.ObjectId `json:"teachers" bson:"teachers"`
	Beacons     []Beacon        `json:"beacons" bson:"beacons"`
	ActiveStart string          `json:"activeStart" bson:"activeStart"`
	ActiveStop  string          `json:"activeStop" bson:"activeStop"`
	Active      bool            `json:"active" bson:"active"`
	State       bool            `json:"state" bson:"state"`
}

//Teacher - user structure
type Teacher struct {
	Login      string        `json:"login" bson:"login"`
	Password   string        `json:"password" bson:"password"`
	Name       string        `json:"name" bson:"name"`
	ID         bson.ObjectId `json:"id" bson:"_id"`
	VK         string        `json:"vk" bson:"vk"`
	FireBaseID string        `json:"fbid" bson:"fbid"`
	VKED       bool          `json:"vked" bson:"vked"`
	Admin      bool          `json:"admin" bson:"admin"`
	Confirmed  bool          `json:"confirmed" bson:"confirmed"`
	Subscribed bool          `json:"subscribed" bson:"subscribed"`
}

//Beacon - beacon structure
type Beacon struct {
	ID     bson.ObjectId `json:"id" bson:"_id"`
	Name   string        `json:"name" bson:"name"`
	State  bool          `json:"state" bson:"state"`
	Active bool          `json:"active" bson:"active"`
}

//Invite - invite link to change status to admin
type Invite struct {
	Key string `bson:"key"`
}

//NewBotPack - generate new bot pack
func NewBotPack(t Teacher, cn string, cmd int) BotPack {
	pack := BotPack{
		Teacher: t,
		Command: cmd,
		Class:   cn}
	return pack
}

//BotPack - bot data package
type BotPack struct {
	Teacher Teacher
	Command int
	Class   string
}

//NotifyerPack - notifyer data pack
type NotifyerPack struct {
	Class   Class
	Command int
}

//NewNotifyerPack - generate new np
func NewNotifyerPack(class Class, cmd int) NotifyerPack {
	return NotifyerPack{
		Class:   class,
		Command: cmd}
}
