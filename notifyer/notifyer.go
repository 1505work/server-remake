package notifyer

import (
	"log"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	cnst "../const"
	s "../structs"
)

//Notifyer - struct for notifyer
type Notifyer struct {
	db            *mgo.Database
	bot           chan<- s.BotPack
	firebase      chan<- s.BotPack
	notifications <-chan s.NotifyerPack
	bl            map[bson.ObjectId]bool
}

//NewNotifyer - new notifyer generator
func NewNotifyer(notiications <-chan s.NotifyerPack, botChan chan<- s.BotPack, fireBaseChan chan<- s.BotPack, db *mgo.Database) Notifyer {
	return Notifyer{
		bot:           botChan,
		firebase:      fireBaseChan,
		notifications: notiications,
		bl:            make(map[bson.ObjectId]bool),
		db:            db}
}

//Start - start service
func (n *Notifyer) Start() {
	go func() {
		log.Println("notifyer started")
		for notification := range n.notifications {
			switch notification.Command {
			case cnst.BAlert:
				if !n.bl[notification.Class.ID] {
					for _, teacher := range notification.Class.Teachers {
						var user s.Teacher
						n.db.C("users").FindId(teacher).One(&user)
						n.notifyList([]s.Teacher{user}, notification)
					}
					n.bl[notification.Class.ID] = true
				}
			case cnst.BThank:
				for _, teacher := range notification.Class.Teachers {
					var user s.Teacher
					n.db.C("users").FindId(teacher).One(&user)
					n.notifyList([]s.Teacher{user}, notification)
				}
				n.bl[notification.Class.ID] = false
			case cnst.BReport:
				for _, teacher := range notification.Class.Teachers {
					var user s.Teacher
					n.db.C("users").FindId(teacher).One(&user)
					n.notifyList([]s.Teacher{user}, notification)
				}
				var admins []s.Teacher
				n.db.C("users").Find(bson.M{"admin": true}).All(&admins)
				notification.Command = cnst.BReportAdmin
				n.notifyList(admins, notification)
				n.bl[notification.Class.ID] = false
			}
		}
	}()
}

func (n *Notifyer) notifyList(teachers []s.Teacher, notification s.NotifyerPack) {
	for _, user := range teachers {
		if user.VKED && user.Confirmed {
			n.bot <- s.NewBotPack(user, notification.Class.Name, notification.Command)
		}
		if user.Subscribed {
			n.firebase <- s.NewBotPack(user, notification.Class.Name, notification.Command)
		}
	}
}
