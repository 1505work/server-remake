package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	var body struct {
		Notification struct {
			Title  string `json:"title"`
			Body   string `json:"body"`
			Action string `json:"click_actio"`
			Icon   string `json:"icon"`
		} `json:"notification"`
		To string `json:"to"`
	}
	body.Notification.Body = "hello world"
	body.Notification.Title = "gachi heh"
	body.Notification.Action = "https://ivanchgeek.ru"
	body.Notification.Icon = "https://ivanchgeek.ru/static/res/favicon.png"
	body.To = "fPlxuaTKI8A:APA91bEg6nOkF_5XnVEetii0WQlVNj5e5B1IDB7-fXqCDZ9zj9_ogpMIviIc25Q9k2JT9RY4I0LUSrJxRuuwyRqwnUWwLdJPDaSEeCTG9jj_Nkz_RxhcaWzQmhrGnrrIe1gD0TulfRSn3"
	jsbody, _ := json.Marshal(body)

	log.Println(string(jsbody))

	request, _ := http.NewRequest("POST", "https://fcm.googleapis.com/fcm/send", bytes.NewBuffer(jsbody))
	request.Header.Set("Authorization", "key=AAAAv3r-HwM:APA91bF_X8zuuc7X0djVuozl2YFKfq96vJ_vPcUzwDMW6OHjDpnE7YT69QKX4eUHtuBjLRjBDzOf_54_wFL-aB1rHHi9Aa5YlcRqS5Sx6krp9G_l6sgrLQa9AjPcasa0DQWcwp0b6HJ1")
	request.Header.Set("Content-Type", "application/json")
	log.Println("---------------------")
	log.Println(request.Header)
	log.Println("---------------------")
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	fmt.Println("response Status:", response.Status)
	fmt.Println("response Headers:", response.Header)
	respbody, _ := ioutil.ReadAll(response.Body)
	fmt.Println("response Body:", string(respbody))
}
